Array.prototype.permShuffle = function() {
    for(let i = this.length - 1; i; i --) {
        const randomIndex = Math.random() * (i + 1) | 0;
        [this[i], this[randomIndex]] = [this[randomIndex], this[i]] // swap
    }
    return this
}

const images = [
    '9a533412-1356-4b89-b74e-1207db6538d6.gif',
    '2020-05-23T20_26_38.265Z.gif',
    '2020-05-24T09_58_03.012Z.gif',
    '2020-05-24T13_34_25.215Z.gif',
    '2020-05-24T21_13_38.626Z.gif',
    '2020-05-26T07_30_06.367Z.gif',
    '2020-05-26T07_47_24.492Z.gif',
    '2020-05-27T16_14_30.486Z.gif',
    '2020-05-28T14_35_45.080Z.gif',
    '2020-05-29T13_49_15.490Z.gif',
    '2020-05-29T21_22_53.935Z.gif',
    '2020-05-29T22_31_27.829Z.gif',
    '2020-05-30T21_37_23.240Z.gif',
    '2020-06-12T22_13_00.621Z.gif',
    '2020-06-14T12_14_41.390Z.gif',
    '2020-06-14T12_20_23.189Z.gif',
    '2020-06-14T17_57_04.939Z.gif',
    '2020-06-15T11_39_34.426Z.gif',
    '2020-06-15T20_09_53.311Z.gif',
    '2020-06-16T20_51_17.464Z.gif',
    '2020-06-16T21_35_18.138Z.gif',
    '2020-06-16T21_56_18.930Z.gif',
    '2020-06-22T19_39_11.970Z.gif',
    '2020-06-22T19_50_50.950Z.gif',
    '2020-06-27T20_10_36.478Z.gif',
    '2020-06-27T20_34_23.204Z.gif',
    '2020-06-27T20_35_19.215Z.gif',
    '2020-06-28T12_59_12.081Z.gif',
    '2020-06-30T10_15_47.614Z.gif',
    '2020-07-04T22_21_57.847Z.gif',
    '2020-07-05T08_07_09.235Z.gif',
    '2020-07-05T08_15_43.168Z.gif',
    '2020-07-05T09_38_12.310Z.gif',
    '2020-07-05T12_10_45.536Z.gif',
    '2020-07-05T12_15_21.717Z.gif',
    '2020-07-05T21_37_50.575Z.gif',
    '2020-07-05T22_16_08.990Z.gif',
    '2020-07-05T22_35_43.483Z.gif',
    '2020-07-06T20_41_47.189Z.gif',
    '2020-07-06T20_52_28.887Z.gif',
    '2020-07-06T21_06_03.604Z.gif',
    '2020-07-06T21_18_41.254Z.gif',
    '2020-07-06T22_04_48.955Z.gif',
    '2020-07-07T07_17_58.273Z.gif',
    '2020-07-08T06_44_12.713Z.gif',
    '2020-07-08T06_44_51.621Z.gif',
    '2020-07-08T06_56_54.490Z.gif',
    '2020-07-08T08_51_48.067Z.gif',
    '2020-07-11T13_14_14.342Z.gif',
    '2020-07-12T21_21_30.553Z.gif',
    '2020-07-12T22_06_02.200Z.gif',
    '2020-07-13T08_40_30.634Z.gif',
    '2020-07-13T19_47_01.589Z.gif',
    '2020-07-13T19_54_03.833Z.gif',
    '2020-07-13T20_55_07.321Z.gif',
    '2020-07-14T14_40_05.049Z.gif',
    '2020-07-14T17_30_51.247Z.gif',
    '2020-07-19T11_21_14.667Z.gif',
    '2020-07-20T15_48_56.382Z.gif',
    '2020-07-25T09_54_53.049Z.gif',
    '2020-07-25T12_27_50.291Z.gif',
    '2020-07-27T14_00_22.287Z.gif',
    '2020-07-29T15_39_56.600Z.gif',
    '2020-08-22T23_37_01.403Z.gif',
    '2020-08-24T08_26_26.163Z.gif',
    '2020-08-24T08_38_32.477Z.gif',
    '2020-08-24T10_36_03.966Z.gif',
    '2020-08-24T10_49_29.011Z.gif',
    '2020-08-24T11_16_10.938Z.gif',
    '2020-08-24T21_26_12.078Z.gif',
    'blob.gif',
    'export.gif',
    'export118.gif',
    'export283.gif',
    'export600.gif',
    'export608.gif',
    'export640_1.gif',
    'export640_2.gif',
    'export734.gif',
    'export834.gif',
    'Fri_May_22_2020 21_47_49.gif',
    'untitled_3.gif',
    'untitled_5.gif',
    'untitled_7.gif',
    'untitled_8.gif',
    'untitled_9.gif',
    'untitled_10.gif',
    'untitled_11.gif',
    'untitled_12.gif',
    'untitled_13.gif',
    'untitled_15.gif',
    'untitled_16.gif',
    'untitled_17.gif',
    'untitled_18.gif',
    'untitled_19.gif',
    'untitled_20.gif',
    'untitled_21.gif',
    'untitled_22.gif',
    'untitled_25.gif',
    'untitled_26.gif',
    'untitled_29.gif',
    'untitled_30.gif',
    'untitled_31.gif',
    'untitled_32.gif',
    'untitled_33.gif',
    'untitled_34.gif',
    'untitled_35.gif',
    'untitled.gif',
].map(d => `images/${d}`)
images.forEach(f => {
    const img = document.createElement('img')
    img.src = f
    document.body.appendChild(img)
})

let index
let cells
const container = document.getElementById('container')

function createGrid() {
    const width = window.innerWidth
    const height = window.innerHeight
    const ny = 2
    const h = height / ny | 0
    const nx = width / h | 0
    const w = h
    const paddingLeft = (width - nx * w) / 2 | 0
    document.body.style.paddingLeft = `${paddingLeft}px`
    container.style.width = `${nx * w}px`
    
    container.innerHTML = ''
    container.style.height = `${ny * h}px`
    container.style.gridTemplateColumns = `${new Array(nx).fill(0).map(d => `${100/nx}%`).join(' ')}`
    
    images.permShuffle()
    index = 0

    for(let i = 0; i < nx * ny; i++) {
        const div = document.createElement('div')
        if(Math.random() < .3) {
            // const N = Math.random() < .75 ? 2 : 3
            const N = 2
            div.classList.add(`grid${N}`)
            for(let j = 0; j < N * N; j ++) {
                const d = document.createElement('div')
                d.classList.add('cell')
                div.appendChild(d)
            }
        }
        else {
            div.classList.add('cell')
        }
        
        container.appendChild(div)
    }

    cells = document.querySelectorAll('.cell')
    cells.forEach(c => c.style.backgroundImage = `url(${images[index++]})`)
}

addEventListener('load', createGrid)
addEventListener('resize', createGrid)

setInterval(() => {
    if(!cells) return;

    // make sure all cels have a background image
    cells.forEach(c => c.style.backgroundImage = c.style.backgroundImage || `url(${images[index++]})`)

    const divs = document.querySelectorAll('#container div:not(.fade)')
    if (divs.length === 0) return;
    const div = divs[Math.random() * divs.length | 0]
    div.classList.add('fade')

    setTimeout(() => div.classList.remove('fade'), 8000)

    setTimeout(() => {
        if(div.parentElement.id === 'container') {
            div.classList.remove('grid2')
            div.classList.remove('grid3')
            div.innerHTML = ''

            if(Math.random() < .25) {
                // const N = Math.random() < .75 ? 2 : 3
                const N = 2
                div.classList.add(`grid${N}`)
                for(let j = 0; j < N * N; j ++) {
                    const d = document.createElement('div')
                    d.classList.add('cell')
                    d.style.backgroundImage = `url(${images[index++ % images.length]})`
                    div.appendChild(d)
                }
            }
            else {
                div.classList.add('cell')
                div.style.backgroundImage = `url(${images[index++ % images.length]})`
            }
        }
        else {
            div.style.backgroundImage = `url(${images[index++ % images.length]})`
        }
    }, 2000)
}, 4000)
